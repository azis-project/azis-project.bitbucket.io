$(document).scroll(function() {
	if ( $(document).scrollTop() >= 70 ) {
		$('.wrap-top-header').addClass('wrap-top-header-hidden');
		$('.wrap-navigasi').addClass('wrap-navigasi-top');
	}else{
		$('.wrap-top-header').removeClass('wrap-top-header-hidden');
		$('.wrap-navigasi').removeClass('wrap-navigasi-top');
	}
});

$('.dropdown').click(function(){
	var id = $(this).data('id');

	$('#'+id).toggleClass('dd-show');
});

$('.dropdown-m').click(function(){
	var id = $(this).data('id');
	$(this).toggleClass('dd-active');
	$('#'+id).slideToggle();
});

$('.nav-open').click(function(){
	$('.side-nav').addClass('side-nav-show');
});

$('.nav-close').click(function(){
	$('.side-nav').removeClass('side-nav-show');
});

$(document).on('click', function(){
	$('.dd-menu').removeClass('dd-show');
});

$('.nav-link').on('click', function(e){
	e.stopPropagation();
});

var swiper = new Swiper(".swiperHome", {
	slidesPerView: "auto",
	spaceBetween: 0,
	loop: true,
	navigation: {
		nextEl: ".home-next",
		prevEl: ".home-prev",
	},
});

var swiper = new Swiper(".swiperTestimoni", {
	slidesPerView: "auto",
	spaceBetween: 18,
	loop: true,
	navigation: {
		nextEl: ".testimoni-next",
		prevEl: ".testimoni-prev",
	},
});

var swiper = new Swiper(".swiperGuru", {
	slidesPerView: "auto",
	spaceBetween: 16,
	loop: true,
	navigation: {
		nextEl: ".guru-next",
		prevEl: ".guru-prev",
	},
});

var swiper = new Swiper(".swiperGuruIn", {
	slidesPerView: "auto",
	spaceBetween: 16,
	loop: true,
	navigation: {
		nextEl: ".guruin-next",
		prevEl: ".guruin-prev",
	},
});

var swiper = new Swiper(".swiperTeam", {
	slidesPerView: "auto",
	spaceBetween: 18,
	loop: true,
	navigation: {
		nextEl: ".team-next",
		prevEl: ".team-prev",
	},
});

$('.list-category-gallery').click(function(){
	var cat = $(this).data('id');
	
	$('.list-gallery').each(function() {
		console.log(cat);
		
		if(cat === 'all' || $(this).data('category') === cat) {
			$(this).addClass('show-gallery');
			$(this).removeClass('hide-gallery');
		} else {
			$(this).removeClass('show-gallery');
			$(this).addClass('hide-gallery');
		}
	});
})