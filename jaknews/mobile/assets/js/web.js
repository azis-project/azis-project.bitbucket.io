$(document).ready(function() {
    stickyNav();
});

$(document).scroll(function() {
	// scrollFunction()
	stickyNav();
});

function scrollFunction() {
	if($(window).scrollTop() > 600) {
		$('.wrap-btn-top').addClass('wrap-btn-top-show');
	} else {
		$('.wrap-btn-top').removeClass('wrap-btn-top-show');
	}
}

function stickyNav(){
	if($(window).scrollTop() > 42) {
		$('.header').addClass('header-sticky');
	}else if($(window).scrollTop() < 42){
		$('.header').removeClass('header-sticky');
	}
}

// Nav Open
$('.btn-nav').click(function(){
	$('body').toggleClass('navside-open');
	$('.btn-nav .fas').toggleClass('fa-times');
});

// Back To Top
$('.wrap-btn-top').click(function(){
	$("html, body").animate({scrollTop: 0}, 500);
});

// Cari Artikel
$('.btn-cari').click(function(){
	$('.pop-search').addClass('pop-show');
	$('body').addClass('modal-open');
	
	$('body').removeClass('navside-open');
	$('.btn-nav .fas').removeClass('fa-times');
});

$('.btn-close').click(function(){
	$('.pop-search').removeClass('pop-show');
	$('body').removeClass('modal-open');
});

$(".btn-cari").click(function(e){
	e.stopPropagation();
});

$('.btn-nav').click(function(e){
	e.stopPropagation();
});

$(".navbar").click(function(e){
	e.stopPropagation();
});

$(".pop-search").click(function(e){
	e.stopPropagation();
});

$(".fullImage").click(function(e){
	e.stopPropagation();
});

$(".pop-foto").click(function(e){
	e.stopPropagation();
});

$(".warp-input-date").click(function(e){
	e.stopPropagation();
});

$(".warp-input-kanal").click(function(e){
	e.stopPropagation();
});

$(".btn-show-date").click(function(e){
	e.stopPropagation();
});

$(".cmb-list-kanal").click(function(e){
	e.stopPropagation();
});

$(".list-kanal").click(function(e){
	e.stopPropagation();
});

$(".pop-content").click(function(e){
	e.stopPropagation();
});

// Blur Body
$('body').click(function(){
	$('.pop-search').removeClass('pop-show');
	$('.pop-kanal').removeClass('pop-show');
	$('.pop-date').removeClass('pop-show');
	$('body').removeClass('modal-open');
	
	$('body').removeClass('navside-open');
	$('.btn-nav .fas').removeClass('fa-times');
})

$('li').on('click','.has-sub', function(){
	var menu = $(this);

	menu.parent().siblings().children('.sub-menu').hide("fast");
	// menu.parent().siblings().removeClass("m-active");

	menu.parent().toggleClass('m-active');

	var subMenu = $(this).parent().children('.sub-menu');
	subMenu.stop(true, true).slideToggle("fast");
})