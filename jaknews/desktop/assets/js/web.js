$(document).ready(function() {
    stickyNav();
});

$(document).scroll(function() {
	// scrollFunction()
	stickyNav();
});

function scrollFunction() {
	if($(window).scrollTop() > 600) {
		$('.wrap-btn-top').addClass('wrap-btn-top-show');
	} else {
		$('.wrap-btn-top').removeClass('wrap-btn-top-show');
	}
}

function stickyNav(){
	if($(window).scrollTop() > 400) {
		$('.navbar').addClass('nav-sticky');
	}else if($(window).scrollTop() < 122){
		$('.navbar').removeClass('nav-sticky');
	}
}

// Back To Top
$('.wrap-btn-top').click(function(){
	$("html, body").animate({scrollTop: 0}, 500);
}) 

// Cari Artikel
$('.btn-cari').click(function(){
	$('.pop-search').addClass('pop-show');
	$('body').addClass('modal-open');
})

$(".btn-cari").click(function(e){
	e.stopPropagation();
});

$(".pop-search").click(function(e){
	e.stopPropagation();
});

$(".warp-input-kanal").click(function(e){
	e.stopPropagation();
});

$(".btn-show-date").click(function(e){
	e.stopPropagation();
});

$(".cmb-list-kanal").click(function(e){
	e.stopPropagation();
});

// Blur Body
$('body').click(function(){
	$('.pop-search').removeClass('pop-show');
	$('body').removeClass('modal-open');
	
	$('.social-share-v').removeClass('social-share-v-show');
	$('.warp-input-kanal').removeClass('open-cmb');
})