/* Desktop */
/* Open Nav Side */
$('.btn-login').click(function(){
	$('.nav-drop').toggleClass('nav-drop-show');
	$('.btn-login').toggleClass('btn-login-active');
});

$(".btn-login").click(function(e){
	e.stopPropagation();
});

$('body').click(function(){
	$('.nav-drop').removeClass('nav-drop-show');
	$('.btn-login').removeClass('btn-login-active');
});

/* Mobile */
/* Open Nav Side */
$("#btn-nav").click(function(e){
	e.stopPropagation();
});

$('#btn-nav').click(function(){
	$('body').addClass('navside-open');
	$('.side-nav').addClass('side-nav-show');
	$('html').css('overflow', 'hidden');
})

/* Close Nav Side */
$('body').click(function(){
	$('body').removeClass('navside-open');
	$('.side-nav').removeClass('side-nav-show');
	$('html').css('overflow', 'auto');
});