window.onscroll = function() {
	navsticky()
	scrollFunction()
	playersticky()
};

// Nav sticky
var navbar 		= document.getElementById("navbar");
var navbarfix 	= document.getElementById("nav-fixed");
var sticky 		= navbar.offsetTop;

function navsticky() {

	if (window.pageYOffset >= 400) {
		navbar.classList.add("sticky");
		navbarfix.classList.add("nav-fixed");
		navbarfix.classList.remove("nav--");
	}
	if (window.pageYOffset <= 200) {
		navbar.classList.remove("sticky");
		navbarfix.classList.remove("nav-fixed");
		navbarfix.classList.add("nav--");
	}
}

function playersticky() {

	if (window.pageYOffset >= 500) {
		$("#floating-player").addClass('floating-player-show');
	}
	if (window.pageYOffset <= 500) {
		$("#floating-player").removeClass('floating-player-show');
	}
}

// Back To Top
btnTop = document.getElementById("Btn2Top");

function scrollFunction() {
	if (document.body.scrollTop > 400 || document.documentElement.scrollTop > 400) {
		btnTop.style.display = "block";
	} else {
		btnTop.style.display = "none";
	}
}

function topFunction() {
	$("html, body").animate({scrollTop: 0}, 500);
}

// Swipper Nav
var swiper = new Swiper('.navSwiper', {
    slidesPerView: 'auto',
    navigation: {
      nextEl: '.nav-next',
      prevEl: '.nav-prev',
    },
});

// Swipper Berita Rekomendasi
var swiper = new Swiper('.rekomendasiSwiper', {
    slidesPerView: 'auto',
    navigation: {
      nextEl: '.rekomendasi-next',
      prevEl: '.rekomendasi-prev',
    },
});

// Swipper Kanal
var swiper = new Swiper('.kanalSwiper', {
    slidesPerView: 'auto',
    navigation: {
      nextEl: '.slide-kanal-next',
      prevEl: '.slide-kanal-prev',
    },
	pagination: {
	  el: ".swiper-pagination",
	},
});

// DatePicker
$('.datepicker').datepicker({
	format: 'dd-mm-yyyy'
});